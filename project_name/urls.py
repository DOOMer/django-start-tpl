# -*- encoding: utf-8 -*-

from django.conf.urls import patterns, include, url

# Comment the next two lines to disable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', '{{ project_name }}.views.home', name='home'),
    # url(r'^{{ project_name }}/', include('{{ project_name }}.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Comment the next line to disable the admin:
    url(r'^admin/', include(admin.site.urls)),
)


handler500 = '{{ project_name }}.apps.core.views.handler500'
handler404 = '{{ project_name }}.apps.core.views.handler404'
