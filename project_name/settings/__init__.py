# -*- encoding: utf-8 -*-

from .admins import *
from .db import *
from .default import *
from .media import *
from .apps import *
from .logs import *

# set default utf0 encode
import sys
reload(sys);
sys.setdefaultencoding("utf8")

try:
    from .local import *
except ImportError:
    pass
