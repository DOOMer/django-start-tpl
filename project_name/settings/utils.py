# -*- encoding: utf-8 -*-

import os.path

PROJECT_ROOT = os.path.dirname(os.path.realpath(__file__)) 
MAIN_ROOT = os.path.dirname(PROJECT_ROOT)
rel = lambda p: os.path.join(PROJECT_ROOT, p)
